package pa.soft.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TrainingunitDAO extends MotherDAO
{
	public TrainingunitDAO()
	{
		super();
	}

	public boolean createUnit(Trainingunit trainingunit)
	{
		String sql = "INSERT INTO trainingschronik (unit_year, unit_month, unit_day, wingtsun, escrima) " +
				"VALUES (" + trainingunit.getUnitYear() + ", " + trainingunit.getUnitMonth() + ", " +
				trainingunit.getUnitDay() + ", '" +
				trainingunit.getWingTsunAsJson() + "', '" +
				trainingunit.getEscrimaAsJson() + "')";

		return insert(sql);
	}

	public List<Year> findYears()
	{
		String sql = "SELECT DISTINCT unit_year FROM trainingschronik ORDER BY unit_year";

		ResultSet result = execute(sql);
		List<Year> years = new ArrayList<>();

		try
		{
			do
			{
				years.add(Year.parse(result.getString(1)));
			} while (result.next());
		}
			catch(SQLException e)
		{
			e.printStackTrace();
		}

		return years;
	}

	public List<Trainingunit> findByYear(Year year)
	{
		String sql = "SELECT * FROM trainingschronik WHERE unit_year = " + year;

		return fetchList(sql);
	}

	public List<String> fetchWingTsunNotes()
	{
		return fetchNotes("wingtsun");
	}

	public List<String> fetchEscrimaNotes()
	{
		return fetchNotes("escrima");
	}

	private List<String> fetchNotes(final String type)
	{
		String sql = "SELECT " + type + " ->> 'note' AS note FROM trainingschronik WHERE " + type + " ->> 'note' IS NOT null";

		ResultSet resultSet = execute(sql);
		Set<String> notes = new HashSet<>();

		try
		{
			do
			{
				notes.add(resultSet.getString("note"));
			} while(resultSet.next());
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}

		return notes.stream()
					.filter(s -> !s.contains("SG"))
					.sorted(String::compareToIgnoreCase)
					.collect(Collectors.toList());
	}

	private List<Trainingunit> fetchList(String sql)
	{
		ResultSet resultSet = execute(sql);
		List<Trainingunit> trainingunits = new ArrayList<>();
		try
		{
			do
			{
				trainingunits.add(
						new Trainingunit(
								resultSet.getString("unit_year"),
								resultSet.getString("unit_month"),
								resultSet.getString("unit_day"),
								resultSet.getString("wingtsun"),
								resultSet.getString("escrima")));
			} while(resultSet.next());
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}

		return trainingunits;
	}
}
