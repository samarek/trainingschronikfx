package pa.soft.model;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Trainingunit
{
	private LocalDate date;

	private Map<String, String> wingTsunMap;
	private Map<String, String> escrimaMap;

	public Trainingunit(String unitYear, String unitMonth, String unitDay, String wingtsun, String escrima)
	{
		date = LocalDate.of(Integer.valueOf(unitYear), Integer.valueOf(unitMonth), Integer.valueOf(unitDay));

		wingTsunMap = parseJsonStringToMap(wingtsun);
		escrimaMap = parseJsonStringToMap(escrima);
	}

	private Map<String, String> parseJsonStringToMap(final String string)
	{
		Map<String, String> out = new HashMap<>();
		out.put("count", "0");

		final String commaOutsideQuotes = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
		final String[] fields = string.replace("{", "").replace("}", "").split(commaOutsideQuotes);
		for(String field : fields)
		{
			String[] keyValue = field.replace("\"", "").trim().split(":");
			out.put(keyValue[0].trim(), keyValue[1].trim());
		}

		return out;
	}

	public int getDay()
	{
		return date.getDayOfMonth();
	}

	public Month getMonth()
	{
		return date.getMonth();
	}

	public Year getYear()
	{
		return Year.of(date.getYear());
	}

	public YearMonth getYearMonth()
	{
		return YearMonth.of(getYear().getValue(), getMonth());
	}

	String getUnitYear()
	{
		return date.getYear() + "";
	}

	String getUnitMonth()
	{
		return date.getMonth().getValue() + "";
	}

	String getUnitDay()
	{
		return date.getDayOfMonth() + "";
	}

	public String getWingTsunKey()
	{
		int rowIndex = (date.getMonth().getValue() * 2) - 1;
		return makeKey(rowIndex);
	}

	public String getTotalWingTsunKey()
	{
		int rowIndex = (date.getMonth().getValue() * 2) - 1;
		return makeTotalKey(rowIndex);
	}

	public String getEscrimaKey()
	{
		int rowIndex = (date.getMonth().getValue() * 2);
		return makeKey(rowIndex);
	}

	public String getTotalEscrimaKey()
	{
		int rowIndex = (date.getMonth().getValue() * 2);
		return makeTotalKey(rowIndex);
	}

	public String getTotalTotalKey()
	{
		int rowIndex = (date.getMonth().getValue() * 2) - 1;
		return makeTotalTotalKey(rowIndex);
	}

	private String makeKey(final int rowIndex)
	{
		int monthOrdinal = date.getMonth().getValue() - 1;
		return monthOrdinal + "_" + rowIndex + "_" + date.getDayOfMonth();
	}

	private String makeTotalKey(final int rowIndex)
	{
		int monthOrdinal = date.getMonth().getValue() - 1;
		return monthOrdinal + "_" + rowIndex + "_total";
	}

	private String makeTotalTotalKey(final int rowIndex)
	{
		int monthOrdinal = date.getMonth().getValue() - 1;
		return monthOrdinal + "_" + rowIndex + "_totalTotal";
	}

	public boolean hasWingTsun()
	{
		return !wingTsunMap.get("count").equals("0");
	}

	public int getWingTsunCount()
	{
		return Integer.valueOf(wingTsunMap.get("count"));
	}

	public String getWingTsunNote()
	{
		String note = "";
		if (wingTsunMap.containsKey("note"))
		{
			note = wingTsunMap.get("note");
		}

		return note;
	}

	public Integer getEscrimaCount()
	{
		return Integer.valueOf(escrimaMap.get("count"));
	}

	public boolean hasEscrima()
	{
		return !escrimaMap.get("count").equals("0");
	}

	public String getEscrimaNote()
	{
		String note = "";
		if (escrimaMap.containsKey("note"))
		{
			note = escrimaMap.get("note");
		}

		return note;
	}

	@Override
	public String toString()
	{
		return "Trainingunit{" +
				"unitYear='" + getYear() + '\'' +
				", unitMonth='" + getMonth() + '\'' +
				", unitDay='" + getDay() + '\'' +
				", wingtsun=" + wingTsunMap +
				", escrima=" + escrimaMap +
				'}';
	}

	String getWingTsunAsJson()
	{
		return makeJsonStringFromMap(wingTsunMap);
	}

	String getEscrimaAsJson()
	{
		return makeJsonStringFromMap(escrimaMap);
	}

	private String makeJsonStringFromMap(final Map<String, String> wingTsunMap)
	{
		List<String> entries = new ArrayList<>();

		wingTsunMap.entrySet().stream()
				   .sorted(Comparator.comparing(Map.Entry::getKey))
				   .forEach(stringStringEntry -> entries.add("\"" + stringStringEntry.getKey() + "\"" + ":" + "\"" + stringStringEntry.getValue() + "\""));

		return "{" + String.join(",", entries) + "}";
	}
}
