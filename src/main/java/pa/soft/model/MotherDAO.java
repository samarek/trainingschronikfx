package pa.soft.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

abstract class MotherDAO
{
	private Map<String, String> dbConfig;
	private Connection connection;

	MotherDAO()
	{
		dbConfig = initConfig();
	}

	private Map<String, String> initConfig()
	{
		String fileName = "db.cfg";

		Map<String, String> out = new HashMap<>();
		try
		{
			FileInputStream fis = new FileInputStream(new File("cfg/" + fileName));
			Scanner scanner = new Scanner(fis);
			String line;
			String[] keyValue;
			while (scanner.hasNext())
			{
				line = scanner.nextLine();
				if (line.trim().length() > 0 && !line.startsWith("#"))
				{
					keyValue = line.split("=");
					out.put(keyValue[0], keyValue[1]);
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		return out;
	}

	private void makeConnection()
	{
		try
		{
			Class.forName("org.postgresql.Driver");
			connection = DriverManager
					.getConnection("jdbc:postgresql://" 	+ dbConfig.get("postgres_host")
									+ ":" + dbConfig.get("postgres_port")
									+ "/" + dbConfig.get("postgres_my_db"),
							dbConfig.get("postgres_me_user"),
							dbConfig.get("postgres_my_password"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	ResultSet execute(String sql)
	{
		makeConnection();
		ResultSet resultSet = null;
		try
		{
			resultSet = connection.prepareStatement(sql).executeQuery();
			resultSet.next();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		closeConnection();

		return resultSet;
	}

	boolean insert(String sql)
	{
		makeConnection();
		boolean result = false;
		try
		{
			result = connection.prepareStatement(sql).execute();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		closeConnection();

		return result;
	}

	private void closeConnection()
	{
		try
		{
			connection.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}
