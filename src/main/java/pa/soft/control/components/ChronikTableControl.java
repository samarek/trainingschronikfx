package pa.soft.control.components;

import javafx.scene.Node;
import pa.soft.control.ViewControl;
import pa.soft.model.TrainingunitDAO;
import pa.soft.view.components.ChronikTableView;

public class ChronikTableControl implements ViewControl
{
	private final ChronikTableView chronikTableView;

	public ChronikTableControl(final TrainingunitDAO trainingunitDAO)
	{
		chronikTableView = new ChronikTableView(trainingunitDAO.findYears(), trainingunitDAO::findByYear);
	}

	public void reload()
	{
		chronikTableView.reload();
	}

	@Override
	public Node getView()
	{
		return chronikTableView.getViewNode();
	}
}
