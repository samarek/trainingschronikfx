package pa.soft.control.components;

import javafx.scene.Node;
import pa.soft.control.ViewControl;
import pa.soft.control.actions.ExportTrainingunitsAction;
import pa.soft.control.actions.ShowUnitAdditionStageAction;
import pa.soft.view.components.ButtonBarView;

public class ButtonBarControl implements ViewControl
{
	private final ButtonBarView viewNode;

	public ButtonBarControl(final ShowUnitAdditionStageAction showUnitAdditionStageAction, final ExportTrainingunitsAction exportTrainingunitsAction)
	{
		viewNode = new ButtonBarView(showUnitAdditionStageAction, exportTrainingunitsAction);
	}

	@Override
	public Node getView()
	{
		return viewNode.getViewNode();
	}
}
