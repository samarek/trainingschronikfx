package pa.soft.control;

import javafx.scene.Node;
import javafx.scene.Parent;
import pa.soft.control.components.ButtonBarControl;
import pa.soft.control.components.ChronikTableControl;
import pa.soft.export.HtmlExporter;
import pa.soft.model.TrainingunitDAO;
import pa.soft.view.MainView;
import pa.soft.view.unitaddition.UnitAdditionStage;

public class MainControl implements ViewControl
{
	private final MainView mainView;

	private MainControl()
	{
		final TrainingunitDAO trainingunitDAO = new TrainingunitDAO();
		final ChronikTableControl chronikTableControl = new ChronikTableControl(trainingunitDAO);

		final HtmlExporter htmlExporter = new HtmlExporter(trainingunitDAO);
		final UnitAdditionStage unitAdditionStage = new UnitAdditionStage(trainingunitDAO, chronikTableControl::reload);
		final ButtonBarControl buttonBarControl = new ButtonBarControl(unitAdditionStage::show, htmlExporter::export);

		mainView = new MainView(chronikTableControl.getView(), buttonBarControl.getView());
	}

	public static Parent start()
	{
		MainControl mainControl = new MainControl();
		return (Parent) mainControl.getView();
	}

	public Node getView()
	{
		return mainView.getViewNode();
	}
}
