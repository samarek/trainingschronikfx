package pa.soft.control;

import javafx.scene.Node;

public interface ViewControl
{
	Node getView();
}
