package pa.soft.control.actions;

import pa.soft.model.Trainingunit;

import java.time.Year;
import java.util.List;

public interface LoadTrainingunitsAction
{
	List<Trainingunit> loadForYear(final Year year);
}
