package pa.soft;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pa.soft.control.MainControl;

public class Main extends Application
{
	@Override
	public void start(Stage primaryStage)
	{
		Parent root = MainControl.start();

		primaryStage.setTitle("Trainingschronik");
		primaryStage.setScene(new Scene(root));
		primaryStage.setMaximized(true);
		primaryStage.show();
	}

	public static void main(String[] args)
	{
		launch(args);
	}
}
