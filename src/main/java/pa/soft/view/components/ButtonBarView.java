package pa.soft.view.components;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import pa.soft.control.actions.ExportTrainingunitsAction;
import pa.soft.control.actions.ShowUnitAdditionStageAction;
import pa.soft.view.ViewNodeContainer;

public class ButtonBarView implements ViewNodeContainer
{
	private final HBox viewNode = new HBox(10);
	private final ExportTrainingunitsAction exportTrainingunitsAction;

	public ButtonBarView(final ShowUnitAdditionStageAction showUnitAdditionStageAction, final ExportTrainingunitsAction exportTrainingunitsAction)
	{
		this.exportTrainingunitsAction = exportTrainingunitsAction;

		makeTrainingUnitAddButton(showUnitAdditionStageAction);
		makeHtmlPrinterButton();

		makeDummyButtons();

		viewNode.getStyleClass().add("button-bar");
		viewNode.getStylesheets().add(getClass().getResource("/css/ButtonBar.css").toExternalForm());

	}

	private void makeTrainingUnitAddButton(final ShowUnitAdditionStageAction showUnitAdditionStageAction)
	{
		Label buttonCaption = new Label("+");
		buttonCaption.getStyleClass().add("button-caption");
		Button trainingunitAddButton = new Button("Einheit hinzufügen", buttonCaption);
		trainingunitAddButton.setContentDisplay(ContentDisplay.TOP);
		trainingunitAddButton.setOnAction(event -> showUnitAdditionStageAction.show());

		viewNode.getChildren().add(trainingunitAddButton);
	}

	private void makeHtmlPrinterButton()
	{
		Label buttonCaption = new Label("HTML");
		buttonCaption.getStyleClass().add("button-caption");
		Button htmlPrinterButton = new Button("HTML exportieren", buttonCaption);
		htmlPrinterButton.setContentDisplay(ContentDisplay.TOP);
		htmlPrinterButton.setOnAction(event -> exportTrainingunits());

		viewNode.getChildren().add(htmlPrinterButton);
	}

	private void exportTrainingunits()
	{
		if(exportTrainingunitsAction.export())
		{
			showInfo("HTML Export erfolgreich");
		}
	}

	private void showInfo(final String text)
	{
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setHeaderText(null);
		alert.setContentText(text);
		alert.showAndWait();
	}

	private void makeDummyButtons()
	{
		for (int i = 0; i < 3; i++)
		{
			Label questionCaption = new Label("?");
			questionCaption.getStyleClass().add("button-caption");
			Button button = new Button("Dummy button", questionCaption);
			button.setContentDisplay(ContentDisplay.TOP);
			viewNode.getChildren().addAll(button);
		}
	}

	@Override
	public Node getViewNode()
	{
		return viewNode;
	}
}
