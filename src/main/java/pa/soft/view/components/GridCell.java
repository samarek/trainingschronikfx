package pa.soft.view.components;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import pa.soft.view.ViewNodeContainer;

public class GridCell implements ViewNodeContainer
{
	private static final String GRID_CELL_STYLE = "grid-cell";
	private static final String NO_DAY_STYLE = "no-day";
	private final StackPane viewNode = new StackPane();
	private final Label caption = new Label();
	private final Label tooltip = new Label("\u2666");
	private final Region evenMonth = new Region();
	private final Region oddMonth = new Region();
	private final Region total = new Region();

	GridCell()
	{
		this("");
	}

	GridCell(String text)
	{
		caption.setText(text);

		viewNode.getChildren().add(caption);
		viewNode.getStyleClass().add(GRID_CELL_STYLE);
		viewNode.getStylesheets().add(getClass().getResource("/css/GridCell.css").toExternalForm());

		tooltip.getStyleClass().add("tooltip-label");
		evenMonth.getStyleClass().add("even-month");
		oddMonth.getStyleClass().add("odd-month");
		total.getStyleClass().add("total");
	}

	void setText(final String text)
	{
		String captionText = text;
		if(captionText.contains(".0") && (captionText.indexOf(".") + 2 >= captionText.length()))
		{
			captionText = captionText.substring(0, captionText.indexOf("."));
		}
		caption.setText(captionText);
	}

	void setTooltip(final String text)
	{
		caption.setTooltip(new Tooltip(text.replaceAll("\"", "")));
		viewNode.getChildren().add(tooltip);
		caption.toFront();
	}

	void setIsEvenMonth(final boolean isEvenMonth)
	{
		if(isEvenMonth)
		{
			viewNode.getChildren().add(evenMonth);
		}
		else
		{
			viewNode.getChildren().add(oddMonth);
		}
		caption.toFront();
	}

	void setToTotal()
	{
		viewNode.getChildren().add(total);
		caption.toFront();
	}

	void isNoDay()
	{
		viewNode.getStyleClass().removeAll(GRID_CELL_STYLE);
		viewNode.getStyleClass().add(NO_DAY_STYLE);
	}

	@Override
	public Node getViewNode()
	{
		return viewNode;
	}
}
