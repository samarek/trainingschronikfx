package pa.soft.view.components;

import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import pa.soft.control.actions.LoadTrainingunitsAction;
import pa.soft.model.Trainingunit;
import pa.soft.view.ViewNodeContainer;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class ChronikTableView implements ViewNodeContainer
{
	private final ComboBox<Year> yearPicker;
	private final GridPane viewNode = new GridPane();
	private final Map<String, GridCell> valueGridCells = new HashMap<>();
	private final LoadTrainingunitsAction loadTrainingunitsAction;

	public ChronikTableView(final List<Year> years, final LoadTrainingunitsAction loadTrainingunitsAction)
	{
		yearPicker = new ComboBox<>(FXCollections.observableArrayList(years));
		this.loadTrainingunitsAction = loadTrainingunitsAction;

		initViewNode();

		makeHeadline();
	}

	private void initViewNode()
	{
		viewNode.setAlignment(Pos.CENTER);
		viewNode.setHgap(1);
		viewNode.setVgap(1);
		viewNode.getStylesheets().add(getClass().getResource("/css/ChronikTable.css").toExternalForm());
	}

	private void makeHeadline()
	{
		final AtomicInteger columnIndex = new AtomicInteger();
		yearPicker.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> displayYear(newValue));
		yearPicker.getSelectionModel().select(yearPicker.getItems().size() - 1);
		yearPicker.getStyleClass().add("year-picker");
		GridPane.setColumnSpan(yearPicker, 2);
		viewNode.addRow(0, yearPicker);
		columnIndex.addAndGet(2);

		makeDaysOfMonths(columnIndex);

		makeGesamtColumnHead(columnIndex);
	}

	private void makeDaysOfMonths(final AtomicInteger columnIndex)
	{
		IntStream.rangeClosed(1, 31)
				 .mapToObj(operand -> new GridCell(operand + ""))
				 .forEach(gridCell -> viewNode.add(gridCell.getViewNode(), columnIndex.getAndIncrement(), 0));
	}

	private void makeGesamtColumnHead(final AtomicInteger columnIndex)
	{
		GridCell gesamtLabel = new GridCell("Gesamt");
		setGridCellsToTotal(gesamtLabel);
		Node gesamtLabelViewNode = gesamtLabel.getViewNode();
		this.viewNode.add(gesamtLabelViewNode, columnIndex.get(), 0);
		GridPane.setColumnSpan(gesamtLabelViewNode, 2);
		columnIndex.addAndGet(2);
	}

	private void displayYear(final Year year)
	{
		resetView();
		List<Trainingunit> trainingunits = loadTrainingunitsAction.loadForYear(year);
		Map<String, Double> totals = new HashMap<>();
		trainingunits.forEach(trainingunit -> {
			displayWingTsun(trainingunit);
			displayEscrima(trainingunit);
			collectTotals(totals, trainingunit);
		});
		collectYearTotals(totals);
		collectAverages(totals, year);

		totals.forEach((key, value) -> {
			String formattedNumber = new DecimalFormat("#.##").format(value);
			valueGridCells.get(key).setText(formattedNumber);
		});
	}

	private void resetView()
	{
		viewNode.getChildren().removeIf(node -> GridPane.getRowIndex(node) > 0);
		valueGridCells.clear();
		displayMonths(1);
		displayYearTotals();
	}

	private void displayWingTsun(final Trainingunit trainingunit)
	{
		if(trainingunit.hasWingTsun())
		{
			final GridCell gridCell = valueGridCells.get(trainingunit.getWingTsunKey());
			gridCell.setText(trainingunit.getWingTsunCount() + "");
			if(!trainingunit.getWingTsunNote().isEmpty())
			{
				gridCell.setTooltip(trainingunit.getWingTsunNote());
			}
		}
	}

	private void displayEscrima(final Trainingunit trainingunit)
	{
		if(trainingunit.hasEscrima())
		{
			final GridCell gridCell = valueGridCells.get(trainingunit.getEscrimaKey());
			gridCell.setText(trainingunit.getEscrimaCount() + "");
			if(!trainingunit.getEscrimaNote().isEmpty())
			{
				gridCell.setTooltip(trainingunit.getEscrimaNote());
			}
		}
	}

	private void collectTotals(final Map<String, Double> totals, final Trainingunit trainingunit)
	{
		int totalCount = 0;
		if(trainingunit.hasWingTsun())
		{
			totals.put(trainingunit.getTotalWingTsunKey(), totals.getOrDefault(trainingunit.getTotalWingTsunKey(), 0d) + trainingunit.getWingTsunCount());
			totalCount += trainingunit.getWingTsunCount();
		}
		if(trainingunit.hasEscrima())
		{
			totals.put(trainingunit.getTotalEscrimaKey(), totals.getOrDefault(trainingunit.getTotalEscrimaKey(), 0d) + trainingunit.getEscrimaCount());
			totalCount += trainingunit.getEscrimaCount();
		}

		totals.put(trainingunit.getTotalTotalKey(), totals.getOrDefault(trainingunit.getTotalTotalKey(), 0d) + totalCount);
	}

	private void collectYearTotals(final Map<String, Double> totals)
	{
		Map<String, Double> yearTotals = new HashMap<>();
		totals.forEach((s, integer) -> {
			Integer rowIndex = Integer.valueOf(s.substring(s.indexOf("_") + 1, s.lastIndexOf("_")));
			String key = ((rowIndex - 1) % 2) + s.substring(s.lastIndexOf("_"));
			yearTotals.put(key, yearTotals.getOrDefault(key, 0d) + integer);
		});
		totals.putAll(yearTotals);
	}

	private void collectAverages(final Map<String, Double> totals, final Year year)
	{
		Map<String, Double> averages = new HashMap<>();

		int relevantDays = isCurrentYear(year) ? LocalDate.now().getDayOfYear() : year.length();
		int relevantMonths = isCurrentYear(year) ? LocalDate.now().getMonthValue() : 12;

		averages.put("0_averagePerDay", totals.getOrDefault("0_total", 0d) / relevantDays);
		averages.put("0_totalAveragePerDay", totals.getOrDefault("0_totalTotal", 0d) / relevantDays);
		averages.put("1_averagePerDay", totals.getOrDefault("1_total", 0d) / relevantDays);

		averages.put("0_averagePerMonth", totals.getOrDefault("0_total", 0d) / relevantMonths);
		averages.put("0_totalAveragePerMonth", totals.getOrDefault("0_totalTotal", 0d) / relevantMonths);
		averages.put("1_averagePerMonth", totals.getOrDefault("1_total", 0d) / relevantMonths);

		totals.putAll(averages);
	}

	private boolean isCurrentYear(final Year year)
	{
		LocalDate now = LocalDate.now();
		return now.getYear() == year.getValue();
	}

	private void displayMonths(int rowIndex)
	{
		for(final Month month : Month.values())
		{
			GridCell gridCell = new GridCell(month.getDisplayName(TextStyle.FULL, Locale.getDefault()));
			boolean isEvenMonth = month.ordinal() % 2 == 1;
			gridCell.setIsEvenMonth(isEvenMonth);
			Node gridCellViewNode = gridCell.getViewNode();
			setRowSpanToTwoOnNodes(gridCellViewNode);
			this.viewNode.addRow(rowIndex, gridCellViewNode);
			makeWingTsunRow(month, rowIndex);
			makeEscrimaRow(month, rowIndex);

			rowIndex += 2;
		}
	}

	private void displayYearTotals()
	{
		makeCaption("WingTsun", 25);
		makeCaption("Escrima", 26);

		displayAveragePerDay();
		displayAveragePerMonth();
		displaySums();
	}

	private void displayAveragePerDay()
	{
		displayAverage("pro Tag", "PerDay", 15);
	}

	private void displayAveragePerMonth()
	{
		displayAverage("pro Monat", "PerMonth", 23);
	}

	private void displayAverage(String caption, String keySuffix, int columnIndex)
	{
		GridCell averageCaption = new GridCell(caption);
		GridCell averageWingTsun = new GridCell();
		GridCell averageEscrima = new GridCell();
		GridCell averageTotal = new GridCell();
		setGridCellsToTotal(averageCaption, averageWingTsun, averageEscrima, averageTotal);
		valueGridCells.put("0_average" + keySuffix, averageWingTsun);
		valueGridCells.put("0_totalAverage" + keySuffix, averageTotal);
		valueGridCells.put("1_average" + keySuffix, averageEscrima);

		Node averageWingTsunViewNode = averageWingTsun.getViewNode();
		Node averageEscrimaViewNode = averageEscrima.getViewNode();
		setColumnSpanToTwoOnNodes(averageWingTsunViewNode, averageEscrimaViewNode);

		Node averageCaptionViewNode = averageCaption.getViewNode();
		Node averageTotalViewNode = averageTotal.getViewNode();
		makeNodesTwoTimesTwo(averageCaptionViewNode, averageTotalViewNode);

		viewNode.add(averageCaptionViewNode, columnIndex, 25);
		viewNode.add(averageWingTsunViewNode, columnIndex + 2, 25);
		viewNode.add(averageEscrimaViewNode, columnIndex + 2, 26);
		viewNode.add(averageTotalViewNode, columnIndex + 4, 25);
	}

	private void displaySums()
	{
		GridCell sumsCaption = new GridCell("Summe");
		GridCell wingTsunYearTotalCount = new GridCell();
		GridCell yearTotalCount = new GridCell();
		GridCell escrimaYearTotalCount = new GridCell();

		setGridCellsToTotal(sumsCaption, wingTsunYearTotalCount, yearTotalCount, escrimaYearTotalCount);

		valueGridCells.put("0_total", wingTsunYearTotalCount);
		valueGridCells.put("0_totalTotal", yearTotalCount);
		valueGridCells.put("1_total", escrimaYearTotalCount);

		Node yearTotalCountViewNode = yearTotalCount.getViewNode();
		setRowSpanToTwoOnNodes(yearTotalCountViewNode);
		Node sumsCaptionViewNode = sumsCaption.getViewNode();
		makeNodesTwoTimesTwo(sumsCaptionViewNode);

		this.viewNode.add(sumsCaptionViewNode, 31, 25);
		this.viewNode.add(wingTsunYearTotalCount.getViewNode(), 33, 25);
		this.viewNode.add(escrimaYearTotalCount.getViewNode(), 33, 26);
		this.viewNode.add(yearTotalCountViewNode, 34, 25);
	}

	private void makeNodesTwoTimesTwo(final Node... sumsCaptionViewNodes)
	{
		setRowSpanToTwoOnNodes(sumsCaptionViewNodes);
		setColumnSpanToTwoOnNodes(sumsCaptionViewNodes);
	}

	private void makeCaption(final String caption, final int rowIndex)
	{
		GridCell wingTsunYearTotalCaption = new GridCell(caption);
		setGridCellsToTotal(wingTsunYearTotalCaption);
		Node wingTsunYearTotalCaptionViewNode = wingTsunYearTotalCaption.getViewNode();
		GridPane.setColumnSpan(wingTsunYearTotalCaptionViewNode, 3);
		this.viewNode.add(wingTsunYearTotalCaptionViewNode, 10, rowIndex);
	}

	private void makeWingTsunRow(final Month month, final int rowIndex)
	{
		makeRow("WingTsun", rowIndex, month);
	}

	private void makeEscrimaRow(final Month month, final int rowIndex)
	{
		makeRow("Escrima", rowIndex + 1, month);
	}

	private void makeRow(final String rowTitle, final int rowIndex, final Month month)
	{
		final AtomicInteger columnIndex = new AtomicInteger(1);
		GridCell gridCell = new GridCell(rowTitle);
		gridCell.setIsEvenMonth(month.ordinal() % 2 == 1);
		viewNode.add(gridCell.getViewNode(), columnIndex.getAndIncrement(), rowIndex);

		addDayCells(rowIndex, month, columnIndex);

		addTotalCell(rowIndex, month, columnIndex);
		if(rowIndex % 2 == 1)
		{
			addTotalTotalCell(rowIndex, month, columnIndex);
		}
	}

	private void addTotalCell(final int rowIndex, final Month month, final AtomicInteger columnIndex)
	{
		GridCell totalCell = makeTotalCell(month);
		String key = month.ordinal() + "_" + rowIndex + "_total";
		valueGridCells.put(key, totalCell);
		this.viewNode.add(totalCell.getViewNode(), columnIndex.getAndIncrement(), rowIndex);
	}

	private void addTotalTotalCell(final int rowIndex, final Month month, final AtomicInteger columnIndex)
	{
		GridCell totalCell = makeTotalCell(month);
		String key = month.ordinal() + "_" + rowIndex + "_totalTotal";
		valueGridCells.put(key, totalCell);
		Node totalCellViewNode = totalCell.getViewNode();
		setRowSpanToTwoOnNodes(totalCellViewNode);
		this.viewNode.add(totalCellViewNode, columnIndex.getAndIncrement(), rowIndex);
	}

	private GridCell makeTotalCell(final Month month)
	{
		GridCell totalCell = new GridCell();
		totalCell.setIsEvenMonth(month.ordinal() % 2 == 1);
		setGridCellsToTotal(totalCell);

		return totalCell;
	}

	private void setRowSpanToTwoOnNodes(final Node... nodes)
	{
		Arrays.stream(nodes).forEach(node -> GridPane.setRowSpan(node, 2));
	}

	private void setColumnSpanToTwoOnNodes(final Node... nodes)
	{
		Arrays.stream(nodes).forEach(node -> GridPane.setColumnSpan(node, 2));
	}

	private void setGridCellsToTotal(final GridCell... gridCells)
	{
		Arrays.stream(gridCells).forEach(GridCell::setToTotal);
	}

	private void addDayCells(final int rowIndex, final Month month, final AtomicInteger columnIndex)
	{
		YearMonth yearMonth = YearMonth.of(yearPicker.getSelectionModel().getSelectedItem().getValue(), month);
		IntStream.rangeClosed(1, 31).forEach(value -> {
			GridCell dayCell = new GridCell();
			if(value > yearMonth.lengthOfMonth())
			{
				dayCell.isNoDay();
			}
			else
			{
				dayCell.setIsEvenMonth(month.ordinal() % 2 == 1);
				String key = month.ordinal() + "_" + rowIndex + "_" + value;
				valueGridCells.put(key, dayCell);
			}
			viewNode.add(dayCell.getViewNode(), columnIndex.getAndIncrement(), rowIndex);
		});
	}

	public void reload()
	{
		displayYear(yearPicker.getValue());
	}

	@Override
	public Node getViewNode()
	{
		return viewNode;
	}
}
