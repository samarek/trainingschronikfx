package pa.soft.view.unitaddition;

import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import pa.soft.control.actions.CloseUnitAdditionStageAction;
import pa.soft.model.Trainingunit;
import pa.soft.model.TrainingunitDAO;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.IntStream;

public class UnitAdditionStage
{
	private final DatePicker datePicker;
	private final Stage viewStage = new Stage();
	private final TrainingunitDAO trainingunitDAO;
	private final CloseUnitAdditionStageAction closeUnitAdditionStageAction;

	private final ComboBox<Integer> wingTsunCount;
	private final ComboBox<String> wingTsunNote;
	private final ComboBox<Integer> escrimaCount;
	private final ComboBox<String> escrimaNote;

	public UnitAdditionStage(final TrainingunitDAO trainingunitDAO, final CloseUnitAdditionStageAction closeUnitAdditionStageAction)
	{
		this.trainingunitDAO = trainingunitDAO;
		this.closeUnitAdditionStageAction = closeUnitAdditionStageAction;

		datePicker = new DatePicker(LocalDate.now());
		wingTsunCount = new ComboBox<>();
		wingTsunNote = new ComboBox<>(FXCollections.observableArrayList(trainingunitDAO.fetchWingTsunNotes()));
		wingTsunNote.setEditable(true);
		HBox.setHgrow(wingTsunNote, Priority.ALWAYS);
		escrimaCount = new ComboBox<>();
		escrimaNote = new ComboBox<>(FXCollections.observableArrayList(trainingunitDAO.fetchEscrimaNotes()));
		escrimaNote.setEditable(true);
		HBox.setHgrow(escrimaNote, Priority.ALWAYS);

		initCountBoxes(wingTsunCount, escrimaCount);

		Scene scene = makeScene();

		viewStage.setScene(scene);
		viewStage.initStyle(StageStyle.UNDECORATED);
		viewStage.focusedProperty().addListener((observable, oldValue, newValue) -> {
			if(oldValue)
			{
				viewStage.close();
				resetInput();
			}
		});
	}

	@SafeVarargs
	private final void initCountBoxes(ComboBox<Integer>... comboBoxes)
	{
		Arrays.stream(comboBoxes).forEach(comboBox -> {
			IntStream.rangeClosed(0, 10).forEach(value -> comboBox.getItems().add(value));
			comboBox.getSelectionModel().select(0);
		});
	}

	private Scene makeScene()
	{
		VBox root = new VBox(10);
		root.setAlignment(Pos.CENTER);

		root.getChildren().add(datePicker);

		root.getChildren().add(makeInputBox("WingTsun", wingTsunCount, wingTsunNote));
		root.getChildren().add(makeInputBox("Escrima", escrimaCount, escrimaNote));

		HBox buttons = createButtons();
		root.getChildren().add(buttons);
		root.getStyleClass().add("unit-addition-stage");
		root.getStylesheets().add(getClass().getResource("/css/UnitAdditionStage.css").toExternalForm());

		return new Scene(root);
	}

	private VBox makeInputBox(final String titel, final Node count, final Node note)
	{
		VBox inputBox = new VBox(5);
		BorderPane captionCountEscrima = new BorderPane();
		Label value = new Label(titel);
		value.getStyleClass().add("input-caption");
		captionCountEscrima.setLeft(value);
		captionCountEscrima.setRight(count);
		inputBox.getChildren().addAll(captionCountEscrima, note);
		note.prefWidth(Double.MAX_VALUE);

		return inputBox;
	}

	private HBox createButtons()
	{
		HBox buttons = new HBox(10);
		Button okButton = new Button("Speichern");
		okButton.setOnAction(event -> saveTrainingUnit());

		Button cancelButton = new Button("Abbrechen");
		cancelButton.setOnAction(event -> viewStage.close());
		buttons.getChildren().addAll(okButton, cancelButton);

		return buttons;
	}

	private void saveTrainingUnit()
	{
		final LocalDate trainingunitTime = datePicker.getValue();
		String unitYear = trainingunitTime.getYear() + "";
		String unitMonth = trainingunitTime.getMonthValue() + "";
		String unitDay = trainingunitTime.getDayOfMonth() + "";
		String wingTsun = makeJsonContent(wingTsunCount, wingTsunNote);
		String escrima = makeJsonContent(escrimaCount, escrimaNote);

		Trainingunit trainingunit = new Trainingunit(unitYear, unitMonth, unitDay, wingTsun, escrima);
		trainingunitDAO.createUnit(trainingunit);

		resetInput();
		viewStage.close();
		closeUnitAdditionStageAction.close();
	}

	private void resetInput()
	{
		datePicker.setValue(LocalDate.now());
		wingTsunCount.getSelectionModel().select(0);
		wingTsunNote.setValue("");
		escrimaCount.getSelectionModel().select(0);
		escrimaNote.setValue("");
	}

	private String makeJsonContent(final ComboBox<Integer> count, final ComboBox<String> note)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("{\"count\":\"").append(count.getValue()).append("\"");
		if(note.getValue() != null && !note.getValue().isEmpty())
		{
			stringBuilder.append(", \"note\":\"").append(note.getValue()).append("\"");
		}
		stringBuilder.append("}");

		return stringBuilder.toString();
	}

	public void show()
	{
		viewStage.show();
	}
}
