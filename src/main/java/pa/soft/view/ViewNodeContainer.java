package pa.soft.view;

import javafx.scene.Node;

public interface ViewNodeContainer
{
	Node getViewNode();
}
