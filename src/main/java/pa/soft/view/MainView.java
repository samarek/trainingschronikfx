package pa.soft.view;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;

public class MainView implements ViewNodeContainer
{
	private final BorderPane viewNode = new BorderPane();

	public MainView(final Node chronikTableControlView, final Node buttonBar)
	{
		viewNode.setTop(buttonBar);

		viewNode.setCenter(chronikTableControlView);

		viewNode.getStylesheets().add(getClass().getResource("/css/Main.css").toExternalForm());
	}

	public Node getViewNode()
	{
		return viewNode;
	}

}
