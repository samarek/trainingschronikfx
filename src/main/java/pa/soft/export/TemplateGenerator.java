package pa.soft.export;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Year;
import java.util.Collection;
import java.util.List;

public class TemplateGenerator
{
	String generateHtmlFrame(final List<Year> years)
	{
		String output = loadTemplate("main.html");
		output = output.replace("_STYLE_", generateStyle());
		output = output.replace("_SCRIPT_", generateScript(years));
		output = output.replace("_HEADER_", generateHeader(years));

		return output;
	}


	private String generateStyle()
	{
		return shortenString(loadTemplate("style.css"));
	}

	private String generateScript(final Collection<Year> years)
	{
		String out = loadTemplate("script.js");
		out = out.replace("_YEARS_", generateYearsForJs(years));

		return shortenString(out);
	}

	private String shortenString(String string)
	{
		return string.replaceAll("\r", "")
					 .replaceAll("\n", "")
					 .replaceAll("\\s+"," ");
	}

	private String generateYearsForJs(final Collection<Year> years)
	{
		StringBuilder yearsStringBuilder = new StringBuilder();

		for (Year year : years)
		{
			if (yearsStringBuilder.length() > 0)
			{
				yearsStringBuilder.append(",");
			}
			yearsStringBuilder.append("\"").append(year).append("\"");
		}
		return yearsStringBuilder.toString();
	}

	private String generateHeader(final Collection<Year> years)
	{
		String out = loadTemplate("header.html");

		out = out.replace("_YEARS_", generateYearPicker(years));
		out = out.replace("_DAYS_", generateHeaderDays());

		return out;
	}

	private String generateYearPicker(final Collection<Year> years)
	{
		StringBuilder options = new StringBuilder("<select id =\"picker\" onchange=\"pick()\">");
		int i = 0;
		String selected = "";
		for (Year year : years)
		{
			if(++i == years.size())
			{
				selected = "selected=\"selected\" ";
			}
			options.append("<option ").append(selected).append("value=\"").append(year).append("\">").append(year).append("</option>");
		}
		options.append("</select>");
		return options.toString();
	}

	private String generateHeaderDays()
	{
		StringBuilder out = new StringBuilder();

		for (int i = 1; i <= 31; i++)
		{
			out.append("<td class=\"headerDay\">").append(i).append("</td>");
		}

		return out.toString();
	}

	String generateFooterTemplate()
	{
		return loadTemplate("footer.html");
	}

	private String loadTemplate(String template)
	{
		Charset encoding = StandardCharsets.UTF_8;
		byte[] encoded = new byte[0];
		try
		{
			encoded = Files.readAllBytes(Paths.get("src/main/resources/templates/" + template ));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		return new String(encoded, encoding);
	}
}
