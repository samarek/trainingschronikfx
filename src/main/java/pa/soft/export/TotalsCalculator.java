package pa.soft.export;

import pa.soft.model.Trainingunit;

import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class TotalsCalculator
{
	private Year year;
	private Map<String, Double> totals;

	public Map<String, Double> calculate(final List<Trainingunit> trainingunits)
	{
		year = trainingunits.get(0).getYear();
		totals = new HashMap<>();
		IntStream.rangeClosed(1, 12).forEach(month -> {
			trainingunits.stream()
						 .filter(Trainingunit::hasWingTsun)
						 .filter(trainingunit -> trainingunit.getMonth().getValue() == month)
						 .forEach(trainingunit -> totals.put(month + "_WingTsun", totals.getOrDefault(month + "_WingTsun", 0d) + trainingunit.getWingTsunCount()));
			trainingunits.stream()
						 .filter(Trainingunit::hasEscrima)
						 .filter(trainingunit -> trainingunit.getMonth().getValue() == month)
						 .forEach(trainingunit -> totals.put(month + "_Escrima", totals.getOrDefault(month + "_Escrima", 0d) + trainingunit.getEscrimaCount()));
			totals.put(month + "_Total", sumFields(month + "_WingTsun", month + "_Escrima"));

			YearMonth yearMonth = YearMonth.of(year.getValue(), month);
			LocalDate now = LocalDate.now();
			int relevantDays = now.getYear() == yearMonth.getYear() && now.getMonthValue() == yearMonth.getMonthValue() ? now.getDayOfMonth() : yearMonth.lengthOfMonth();
			totals.put(month + "_Day_WingTsun", totals.getOrDefault(month + "_WingTsun", 0d) / relevantDays);
			totals.put(month + "_Day_Escrima", totals.getOrDefault(month + "_Escrima", 0d) / relevantDays);
			totals.put(month + "_Day", sumFields(month + "_Day_WingTsun", month + "_Day_Escrima"));
		});


		calculateTotal("WingTsun");
		calculateTotal("Escrima");
		totals.put("Total", sumFields("Total_WingTsun", "Total_Escrima"));

		calculateAveragePerDay("WingTsun");
		calculateAveragePerDay("Escrima");
		totals.put("Total_Day", sumFields("Day_WingTsun", "Day_Escrima"));

		calculateAveragePerMonth("WingTsun");
		calculateAveragePerMonth("Escrima");
		totals.put("Total_Month", sumFields("Month_WingTsun", "Month_Escrima"));


		return totals;
	}

	private void calculateTotal(final String string)
	{
		final Double[] sum = {0d};
		IntStream.rangeClosed(1, 12).forEach(month -> sum[0] += totals.getOrDefault(month + "_" + string, 0d));
		totals.put("Total_" + string, sum[0]);
	}

	private void calculateAveragePerDay(final String string)
	{
		LocalDate now = LocalDate.now();
		int relevantDays = year.getValue() == now.getYear() ? now.getDayOfYear() : year.length();

		totals.put("Day_" + string, totals.getOrDefault("Total_" + string, 0d) / relevantDays);
	}

	private void calculateAveragePerMonth(final String string)
	{
		LocalDate now = LocalDate.now();
		int relevantMonths = year.getValue() == now.getYear() ? now.getMonthValue() : 12;

		totals.put("Month_" + string, totals.getOrDefault("Total_" + string, 0d) / relevantMonths);
	}

	private double sumFields(String... args)
	{
		final double[] sum = {0d};

		Arrays.stream(args).forEach(s -> sum[0] += totals.getOrDefault(s, 0d));

		return sum[0];
	}
}
