package pa.soft.export;

import pa.soft.model.Trainingunit;
import pa.soft.model.TrainingunitDAO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.TextStyle;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class HtmlExporter
{
	private final static String NO_VALUE = "<td class=\"day\"></td>";
	private final static String NO_DAY = "<td align=\"center\" class=\"empty day\"></td>";
	private Year currentYear;
	private Month currentMonth;
	private final DecimalFormat decimalFormat = new DecimalFormat("#.##");
	private final TrainingunitDAO trainingunitDAO;
	private final TemplateGenerator templateGenerator = new TemplateGenerator();
	private final TotalsCalculator totalsCalculator = new TotalsCalculator();

	public HtmlExporter(final TrainingunitDAO trainingunitDAO)
	{
		this.trainingunitDAO = trainingunitDAO;
	}

	public boolean export()
	{
		List<Year> years = trainingunitDAO.findYears();

		StringBuilder content = new StringBuilder();
		years.forEach(year -> content.append(generatePage(year)));

		String htmlFrame = templateGenerator.generateHtmlFrame(years);

		return writeToFile(htmlFrame.replace("_CONTENT_", content.toString()));
	}

	private String generatePage(final Year year)
	{
		currentYear = year;
		List<Trainingunit> trainingunits = trainingunitDAO.findByYear(year);
		Map<String, Double> totals = totalsCalculator.calculate(trainingunits);

		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append(generateMonths(trainingunits, totals));
		stringBuilder.append(generateTotals(totals));

		return stringBuilder.toString();
	}

	private String generateMonths(final List<Trainingunit> trainingunits, final Map<String, Double> totals)
	{
		StringBuilder months = new StringBuilder();
		IntStream.rangeClosed(1, 12).forEach(value -> {
			currentMonth = Month.of(value);
			months.append(generateMonthRow(trainingunits.stream()
														.filter(trainingunit -> trainingunit.getMonth().getValue() == value)
														.sorted(Comparator.comparing(Trainingunit::getDay))
														.collect(Collectors.toList()), totals));
		});

		return months.toString();
	}

	private String generateMonthRow(final List<Trainingunit> trainingunits, final Map<String, Double> totals)
	{
		YearMonth yearMonth = YearMonth.of(currentYear.getValue(), currentMonth.getValue());
		StringBuilder wingTsunRow = new StringBuilder(initWingTsunRow(yearMonth));
		StringBuilder escrimaRow = new StringBuilder(initEscrimaRow(yearMonth));
		IntStream.rangeClosed(1, 31).forEach(day -> {
			if(day <= yearMonth.lengthOfMonth())
			{
				Optional<Trainingunit> optionalTrainingunit = trainingunits.stream().filter(trainingunit -> trainingunit.getDay() == day).findAny();
				if(optionalTrainingunit.isPresent())
				{
					wingTsunRow.append(makeWingTsunField(optionalTrainingunit.get()));
					escrimaRow.append(makeEscrimaField(optionalTrainingunit.get()));
				}
				else
				{
					wingTsunRow.append(NO_VALUE);
					escrimaRow.append(NO_VALUE);
				}
			}
			else
			{
				wingTsunRow.append(NO_DAY);
				escrimaRow.append(NO_DAY);
			}
		});

		wingTsunRow.append(finishWingTsunRow(totals));
		escrimaRow.append(finishEscrimaRow(totals));

		return wingTsunRow.toString() + escrimaRow.toString();
	}

	private String initWingTsunRow(final YearMonth yearMonth)
	{
		String rowClass = (yearMonth.getMonthValue() % 2 == 0) ? "evenH" : "oddH";

		return "<tr class=\"" + rowClass + "\" name=\"" + yearMonth.getYear() + "\">" +
				"<td rowspan=\"2\">" + yearMonth.getMonth().getDisplayName(TextStyle.FULL, Locale.GERMAN) + "</td>" +
				"<td>WingTsun</td>";
	}

	private String initEscrimaRow(final YearMonth yearMonth)
	{
		String rowClass = (yearMonth.getMonthValue() % 2 == 0) ? "evenH" : "oddH";

		return "<tr class=\"" + rowClass + "\" name=\"" + yearMonth.getYear() + "\">" +
				"<td>Escrima</td>";
	}

	private String makeWingTsunField(final Trainingunit trainingunit)
	{
		return generateTrainingUnitField(trainingunit.getWingTsunCount(), trainingunit.getWingTsunNote());
	}

	private String makeEscrimaField(final Trainingunit trainingunit)
	{
		return generateTrainingUnitField(trainingunit.getEscrimaCount(), trainingunit.getEscrimaNote());
	}

	private static String generateTrainingUnitField(Integer count, String note)
	{
		String field = "<td class=\"day\"></td>";
		if (count > 0)
		{
			String styleClass = ">";
			if (note.length() > 0)
			{
				styleClass = " class=\"note day\"><span class=\"notetext\">" + note.replace("\"", "") + "</span>";
			}
			field = "<td align=\"center\"" + styleClass + count + "</td>";
		}

		return field;
	}

	private String finishWingTsunRow(final Map<String, Double> totals)
	{
		String wingTsunRow = "<td align=\"center\" class=\"sum single\">" + decimalFormat.format(totals.getOrDefault(currentMonth.getValue() + "_Day_WingTsun", 0d)) + "</td>" +
				"<td align=\"center\" class=\"sum total\" rowspan=\"2\">" + decimalFormat.format(totals.getOrDefault(currentMonth.getValue() + "_Day", 0d)) + "</td>";
		wingTsunRow += "<td align=\"center\" class=\"sum single\">" + decimalFormat.format(totals.getOrDefault(currentMonth.getValue() + "_WingTsun", 0d)) + "</td>" +
				"<td align=\"center\" class=\"sum total\" rowspan=\"2\">" + decimalFormat.format(totals.getOrDefault(currentMonth.getValue() + "_Total", 0d)) + "</td></tr>\n";

		return wingTsunRow;
	}

	private String finishEscrimaRow(final Map<String, Double> totals)
	{
		String escrimaRow = "<td align=\"center\" class=\"sum single\">" + decimalFormat.format(totals.getOrDefault(currentMonth.getValue() + "_Day_Escrima", 0d)) + "</td>";
		escrimaRow += "<td align=\"center\" class=\"sum single\">" + decimalFormat.format(totals.getOrDefault(currentMonth.getValue() + "_Escrima", 0d)) + "</td></tr>\n";

		return escrimaRow;
	}

	private String generateTotals(final Map<String, Double> totals)
	{
		String out = templateGenerator.generateFooterTemplate();

		out = out.replace("_YEAR_", currentYear + "");

		out = out.replace("_PER_DAY_WINGTSUN_", decimalFormat.format(totals.get("Day_WingTsun")));
		out = out.replace("_PER_MONTH_WINGTSUN_", decimalFormat.format(totals.get("Month_WingTsun")));
		out = out.replace("_TOTAL_WINGTSUN_", decimalFormat.format(totals.get("Total_WingTsun")));

		out = out.replace("_PER_DAY_ESCRIMA_", decimalFormat.format(totals.get("Day_Escrima")));
		out = out.replace("_PER_MONTH_ESCRIMA_", decimalFormat.format(totals.get("Month_Escrima")));
		out = out.replace("_TOTAL_ESCRIMA_", decimalFormat.format(totals.get("Total_Escrima")));

		out = out.replace("_PER_DAY_TOTAL_", decimalFormat.format(totals.get("Total_Day")));
		out = out.replace("_PER_MONTH_TOTAL_", decimalFormat.format(totals.get("Total_Month")));
		out = out.replace("_TOTAL_", decimalFormat.format(totals.get("Total")));

		return out;
	}

	private boolean writeToFile(String input)
	{
		try
		{
			FileWriter fileWriter = new FileWriter(new File("out/trainingschronik.html"));

			fileWriter.write(input + "\n");
			fileWriter.flush();
			fileWriter.close();
			return true;
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
