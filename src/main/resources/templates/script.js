function pick()
{
    var years = [_YEARS_];
    for (var i = 0; i < years.length; i++)
    {
        var turnOff = document.getElementsByName(years[i]);
        for(var j = 0; j < turnOff.length; j++)
        {
            turnOff[j].style.display = 'none';
        }
    }

    var selectBox = document.getElementById('picker');
    var selection = selectBox.options[selectBox.selectedIndex].value;
    var turnOn = document.getElementsByName(selection);
    for(var i = 0; i < turnOn.length; i++)
    {
        turnOn[i].style.display = 'table-row';
    }
}