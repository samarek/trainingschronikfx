CREATE USER me WITH PASSWORD 'ChangeMe';
CREATE DATABASE mine OWNER me ENCODING 'UTF-8';

DROP TABLE IF EXISTS trainingschronik;
CREATE SEQUENCE trainingschronik_id_seq;
CREATE TABLE trainingschronik (
  id         INTEGER DEFAULT nextval('trainingschronik_id_seq') PRIMARY KEY,
  unit_year  INTEGER NOT NULL,
  unit_month INTEGER NOT NULL,
  unit_day   INTEGER NOT NULL,
  wingtsun   JSON,
  escrima    JSON,
  UNIQUE (unit_year, unit_month, unit_day)
);
ALTER SEQUENCE trainingschronik_id_seq OWNED BY trainingschronik.id;
ALTER TABLE trainingschronik
  OWNER TO me;
